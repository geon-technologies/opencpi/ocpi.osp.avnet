# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.avnet/-/compare/52591ce4...v2.4.2) (2022-05-26)

Initial release of MicroZed & PicoZed OSPs
