### Proposal
(Describe your idea and its benefit(s).  An idea is typically a general
direction that could result in new features or enhancements, but not defined
enough to make a feature or enhancement request.)

### Intended users
(Who could benefit from the implementation of this idea?)

### Links/references

### Acceptance criteria
(What would it take for you to feel this issue can be closed?)
