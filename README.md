[//]: # (These are reference links used in the body of this note and get 
         stripped out when the markdown processor does its job. The blank lines
         before and after these references are important for portability across
         different markdown parsers.)


[avnet-docs]:  <https://opencpi.gitlab.io/releases/latest/rst/osp_avnet>

# avnet

## Provided 
---
The following is provided within this repo:  
- Microzed OSPs  
    - Microzed 7020 + carrier card (CC)
    - Microzed 7010 + CC   
- OSP Development Guide using the Picozed 7020 + CC as a template  
- Picozed OSPs  
    - Picozed 7030 + CC   
    - Picozed 7020 + CC   
    - Picozed 7015 + CC   
    - Picozed 7010 + CC   
- OSP Development Guide using the Picozed 7030 + CC as a template  
- Code blocks for each design stage of the Development Guide as a reference   

## Getting started (MicroZed)
---
See the getting started guide for each MicroZed platform located in hdl/platforms/microzed_xx_cc/microzed_xx_cc-gsg.rst (source) and
on [opencpi.gitlab.io][avnet-docs] (HTML)

## Guide to developing an OSP for Zynq-7000 based platforms (MicroZed)
---
See Guide.md located in the guide/microzed_cc/ directory

## Getting started (PicoZed)
---
See the getting started guide for each PicoZed platform located in hdl/platforms/picozed_xx_cc/picozed_xx_cc-gsg.rst (source) and on [opencpi.gitlab.io][avnet-docs] (HTML)

## Guide to developing an OSP for Zynq-7000 based platforms (PicoZed)
---
See Guide.md located in the guide/picozed_cc/ directory  

## Version tested against
---
These guides and the associated OSP were tested against and are compatible with the following OpenCPI version:  
v2.4.2

Notes:
---
- hdl/assemblies - contains copies of assemblies from the OpenCPI assets project,
  with the addition of MicroZed and PicoZed SOM-specific container XML files.
  A desirable framework improvement would be to allow the use of containers
  defined within the OSP in the appropriate assemblies without having to
  copy the assemblies into the OSP.
