# ----------------------------------------------------------------------------
#     _____
#    /     \
#   /____   \____
#  / \===\   \==/
# /___\===\___\/  AVNET ELECTRONICS MARKETING
#      \======/         www.em.avnet.com/drc
#       \====/    
# ----------------------------------------------------------------------------
# 
#  Created With Avnet Constraints Generator V0.8.0 
#     Date: Tuesday, September 02, 2014 
#     Time: 10:33:25 PM 
# 
#  This design is the property of Avnet.  Publication of this
#  design is not authorized without written consent from Avnet.
#  
#  Please direct any questions to:
#     MicroZed.org Community Forums
#     http://www.microzed.org
# 
#  Disclaimer:
#     Avnet, Inc. makes no warranty for the use of this code or design.
#     This code is provided  "As Is". Avnet, Inc assumes no responsibility for
#     any errors, which may appear in this code, nor does it make a commitment
#     to update the information contained herein. Avnet, Inc specifically
#     disclaims any implied warranties of fitness for a particular purpose.
#                      Copyright(c) 2014 Avnet, Inc.
#                              All rights reserved.
# 
# ----------------------------------------------------------------------------
# 
#  Notes: 
#
#  20 April 2015
#     IO standards based upon Bank 34 and Bank 35 (and Bank 13) Vcco supply 
#     options of 1.8V, 2.5V, or 3.3V are possible based upon the Vadj 
#     jumper (J18) settings.  By default, Vadj is expected to be set to 
#     1.8V but if a different voltage is used for a particular design, then 
#     the corresponding IO standard within this UCF should also be updated 
#     to reflect the actual Vadj jumper selection.
#
#     Net names are not allowed to contain hyphen characters '-' since this
#     is not a legal VHDL87 or Verilog character within an identifier.  
#     HDL net names are adjusted to contain no hyphen characters '-' but 
#     rather use underscore '_' characters.  Comment net name with the hyphen 
#     characters will remain in place since these are intended to match the 
#     schematic net names in order to better enable schematic search.
#
#     The string provided in the comment field provides the Zynq device pin 
#     mapping through the expansion connector to the carrier card net name
#     according to the following format:
#
#     "<Zynq Pin>.<SOM Net>.<Connector Ref>.<Connector Pin>.<Carrier Net>"
# 
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# User LEDs 
# ---------------------------------------------------------------------------- 
set_property PACKAGE_PIN R19 [get_ports [list leds[0]]];  # "PL_LED1"
set_property PACKAGE_PIN V13 [get_ports [list leds[1]]];  # "PL_LED2"
set_property PACKAGE_PIN K16 [get_ports [list leds[2]]];  # "PL_LED3"
set_property PACKAGE_PIN M15 [get_ports [list leds[3]]];  # "PL_LED4"

set_property IOSTANDARD LVCMOS33 [get_ports leds[0]];
set_property IOSTANDARD LVCMOS33 [get_ports leds[1]];
set_property IOSTANDARD LVCMOS33 [get_ports leds[2]];
set_property IOSTANDARD LVCMOS33 [get_ports leds[3]];
